import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CarHistoryService } from '../_services/car-history.service';
import { History } from '../_models/history';
import { UserService } from '../_services/user.service';
import { User } from '../_models/user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-car-history',
  templateUrl: './add-car-history.component.html',
  styleUrls: ['./add-car-history.component.css']
})
export class AddCarHistoryComponent implements OnInit {

  loading = false;
  submitted = false;
  currentUser: User;


  name:string;
  lastName:string;
  companyName:string;
  data: string;
  price: number;
  serviceName: string;
  description: string;



  constructor(
    private userService: UserService,
    private carHistoryService: CarHistoryService,
    private router: Router) { }

  ngOnInit() {
    this.userService.getByUsername(localStorage.getItem('currentUsername')).pipe().subscribe(user => this.currentUser = user);
  }

  addHistory() {
    this.submitted = true;
    const historyToAdd = new History(
      this.currentUser.id,
      this.name,
      this.lastName,
      this.companyName,
      this.data,
      this.price,
      this.serviceName,
      this.description
    );

    this.carHistoryService.addHistory(historyToAdd).subscribe(
      data => {
        this.router.navigate(['/']);
      },
      error => {
          console.log(error);
          this.loading = false;
      });

    this.loading = true;
  }
}
