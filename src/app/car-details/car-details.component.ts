import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import * as Chartist from 'chartist';

import { Car } from '../_models/car';
import { CarService } from '@app/_services/car.service';
import {CarHistoryService} from '../_services/car-history.service';
import {UserService} from '../_services/user.service';
import { User } from '@app/_models/user';


@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.css']
})
export class CarDetailsComponent implements OnInit {

  car: Car;
  currentUser: User;
  loading = false;
  submitted = false;

  name:string;
  model: string;
  serialNumber: number;
  volume: number;
  color: string;
  year: number;
  city: string;
  country: string;
  about: string;

  carToedit: Car;
  id: number;

  constructor(
    private route: ActivatedRoute,
    private carService: CarService,
    private userService: UserService,
    private location: Location,
    private router: Router
  ) {}

  ngOnInit() {
  this.id = +this.route.snapshot.paramMap.get('id');
  this.getUser();

}


onSubmit() {
}
  getUser() {
    this.userService.getByUsername(localStorage.getItem('currentUsername')).pipe().subscribe(user => {
      this.currentUser = user;
      this.getCar();
    });

  }

  getCar(): void {

       this.carService.getCarById(this.id + '').toPromise().then(c => {
          this.car = c;
          if (this.car.photo == null) {
            this.car.photo = 'assets/img/nocar1.jpg';
          }
       }).catch(e => {
         console.error(e);
       });
  }

  editCar() {

    this.carToedit  = new Car(
    this.car.id,
    this.currentUser.id,
    this.name,
    this.model,
    this.serialNumber,
    this.volume,
    this.color,
    this.year,
    this.city,
    this.country,
    this.about );

    this.submitted = true;
      this.carService.editCar(this.carToedit)
              .subscribe(
                  data => {
                    this.router.navigate(['/']);
                  },
                  error => {
                      console.log(error);
                      this.loading = false;
                  });
      this.loading = true;

  }

  goBack(): void {
    this.location.back();
  }

}
