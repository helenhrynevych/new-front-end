import { Component, OnInit } from '@angular/core';
import { Car } from '../_models/car';
import { User } from '../_models/user';
import { CarService } from '../_services/car.service';
import { UserService } from '../_services/user.service';


@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  carsList: Car[];
  currentUser: User;
  loading = false;
  submitted = false;

  constructor(
    private carService: CarService,
    private userService: UserService) { }

  ngOnInit() {
    this.getUser();

  }

  getUser() {
    this.userService.getByUsername(localStorage.getItem('currentUsername')).pipe().subscribe(user => {
      this.currentUser = user;
      this.getCarsFromDB(user.id);
    });

  }


  getCarsFromDB(id: number) {

    this.carService.getCarListByUserId(id).
    subscribe(data => { this.carsList = data; });
  }



}
