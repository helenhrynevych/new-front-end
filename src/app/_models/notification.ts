export class Notification {
  content: String;
  token: String;

  constructor(content: String, token: String) {
    this.content = content;
    this.token = token;
  }
}
