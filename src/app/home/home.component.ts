﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../_services/user.service';
import { User } from '../_models/user';
import { Router } from '@angular/router';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit {
    currentUser: User;
    file: File;
    loading = false;
    submitted = false;
    public imagePath;
    imgURL: any;
    public profilePath;
    profileURL: any;
  //   clienID = 'a91d10bd0d1b991';
  //   $scope.uploader = new FileUploader({
  //     url: 'https://api.imgur.com/3/image',
  //     alias: 'image',
  //     headers: {
  //         Authorization: 'Client-ID a91d10bd0d1b991'
  //     },
  //     autoUpload: true
  // });




    constructor( private userService: UserService,
       private router: Router) {
    }

    ngOnInit() {
      this.userService.getByUsername(localStorage.getItem('currentUsername')).pipe().subscribe(user => this.currentUser = user);       
    }

    onSubmit(){
      this.submitted = true;
      this.userService.update(this.currentUser)
              .subscribe(
                  data => {
                    this.router.navigate(['/']);
                  },
                  error => {
                      console.log(error);
                      this.loading = false;
                  });
      this.loading = true;
    }


    changeBackground(files){
      if(files.length === 0){
        return;
      }
      const mimeType = files[0].type;
      if(mimeType.match( /image\/*/) == null){
        console.log('only images');
        return;
      }

      var reader = new FileReader();
      this.imagePath = files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) =>{
      this.imgURL = reader.result;
      }
    }


    changeProfilePhoto(files){
      if(files.length === 0){
        return;
      }
      const mimeType = files[0].type;
      if(mimeType.match( /image\/*/) == null){
        console.log('only images');
        return;
      }

      var reader = new FileReader();
      this.profilePath = files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) =>{
      this.profileURL = reader.result;
      }
    }


    deleteUser(id: number) {

    }
}