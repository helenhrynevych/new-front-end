export class History{
    id:number;
    userId:number;
    name:string;
    lastName:string;
    companyName:string;
    serviceName:string;
    description:string;
    data:String;
    price:number;

    constructor(
        userId:number,
        name:string,
        lastName:string,
        companyName:string,
        data: string,
        price: number,
        serviceName: string,
        description: string
    ){
        this.userId=userId;
        this.name=name;
        this.lastName=lastName;
        this.companyName=companyName;
        this.data=data;
        this.price=price;
        this.serviceName=serviceName;
        this.description=description;
    }
}