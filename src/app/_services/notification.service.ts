import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Notification } from '../_models/notification';
import { environment } from '../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { MessagingService } from '../_services/messaging.service';

@Injectable({ providedIn: 'root' })
export class NotificationService {
  constructor(private http: HttpClient, private toastr: ToastrService) {}

  sendNotification(notification: Notification) {
    return this.http
      .post(`${environment.backendUrl}/api/notification/send`, notification)
      .toPromise()
      .then(() => {
        this.toastr.success('Wysłano powiadomienie');
      })
      .catch(() => {
        this.toastr.error('Błąd podczas wysyłania powiadomienia');
      });
  }
}
