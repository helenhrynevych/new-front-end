import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Session } from 'protractor';

const TOKEN_KEY = 'AuthToken';

@Injectable()
export class TokenStorage {

  constructor(private router: Router) { }

  signOut() {
    // sessionStorage.clear();
    localStorage.clear();
  }

  public saveToken(token: string) {
    // sessionStorage.setItem(TOKEN_KEY,  token);
    localStorage.setItem(TOKEN_KEY,  token);
  }

  public getToken(): string {
    // return sessionStorage.getItem(TOKEN_KEY);
    return localStorage.getItem(TOKEN_KEY);
  }
}
