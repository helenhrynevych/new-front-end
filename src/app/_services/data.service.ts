import {HttpClient, HttpParams} from "@angular/common/http";
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import { environment } from '@environments/environment';

@Injectable()
export class DataService {

  constructor(private http : HttpClient){
  }

  createEvent(data: CreateEventParams): Observable<EventData> {
    return this.http.post(environment.url+"/api/events/create", data) as Observable<any>;
  }

  getResources(): Observable<any> {
    return this.http.get(environment.url + '/api/resource') 
  }


  getHistoryListByUserId(id: number): Observable<any> {
    return this.http.get<History[]>(environment.url + '/history/getList/' + id);
  }

  getEvents(id : number): Observable<any>{
    //let p = new HttpParams().set('id', userId);
    return this.http.get<any[]>(environment.url + '/api/events/'+id)
  }


  moveEvent(data: any): Observable<EventData> {
    return this.http.post(environment.url+"/api/events/move", data) as Observable<any>;
  }

}

export interface MoveEventParams {
    id: string | number;
    start: string;
    end: string;
    resource: string | number;
  }
  
  export interface EventData {
    id: string | number;
    start: string;
    end: string;
    text: string;
    resource: string | number;
  }

  
export interface CreateEventParams {
    start: string;
    end: string;
    text: string;
    resource: string | number;
    user: number;
  }
  
  export interface EventData {
    id: string | number;
    start: string;
    end: string;
    text: string;
    resource: string | number;
  }