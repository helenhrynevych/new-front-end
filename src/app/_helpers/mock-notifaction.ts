const showNotification = (body: string) => {
  const options = {
    body
  };
  const notification = new Notification('CarApp', options);
  notification.onclick = (e: Event) => {
    e.preventDefault();
    window.open('http://localhost:4200/');
  };
};

const mockNotification = (content: string) => {
  // Let's check if the browser supports notifications
  if (!('Notification' in window)) {
    alert('This browser does not support desktop notification');
  } else if (Notification.permission === 'granted') {
    // If it's okay let's create a notification
    showNotification(content);
  } else if (Notification.permission !== 'denied') {
    Notification.requestPermission().then(permission => {
      if (permission === 'granted') {
        showNotification(content);
      }
    });
  }
};

export default mockNotification;
