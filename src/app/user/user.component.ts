import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../_models/user';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  currentUser: User;
  quot: string = 'I like the way you work it';
  loading = false;
  submitted = false;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userService.getByUsername(localStorage.getItem('currentUsername')).pipe().subscribe(user => this.currentUser = user);

    if(this.currentUser.quotation != null) {
      this.quot = this.currentUser.quotation;
    }
  }


  onSubmit(){
    this.submitted = true;
    this.userService.update(this.currentUser)
            .subscribe(
                data => {
                  this.router.navigate(['/']);
                },
                error => {
                    console.log(error);
                    this.loading = false;
                });

    this.loading = true;
  }

  onFileChanged(event){
    console.log("************")
    console.log(event);
    const file = event.target.files[0];
    console.log("photo: ");
    console.log(file);
  }

  getBackground(){
    if (this.currentUser.background == null) {
      return 'assets/img/background.jpg';
    }

    return this.currentUser.background;
  }

  getPhoto(){
    if (this.currentUser.photo == null) {
      return 'assets/img/nofoto.jpg';
    }

    return this.currentUser.photo;
  }

  

}
