﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TokenStorage } from '../_helpers/token.storage';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    
    constructor(private http: HttpClient,private token: TokenStorage) {
    }

    public isAuthenticated(): boolean {
        const token = this.token.getToken();
        return token != null;
      }

    login(username: string, password: string) {
                return this.http.post<any>(environment.url + '/api/auth/signin', {username, password});
        }

    logout() {
        this.token.signOut();
    }
}