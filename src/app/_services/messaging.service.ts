import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { mergeMapTo } from 'rxjs/operators';
import { take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class MessagingService {

  currentMessage = new BehaviorSubject(null);
  token: String;

  constructor(
    private angularFireDB: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private angularFireMessaging: AngularFireMessaging
  ) {
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    );
  }

  /** updatae token in firebase
   * @param userId userid as key
   * @param token token as value
   */

  updateToken(userId, token) {
    this.angularFireAuth.authState.pipe(take(1)).subscribe(
      () => {
        const data = {};
        data[userId] = token;
        this.angularFireDB.object('fcmTokens/').update(data);
      });
  }

  /** request perm for notification from fireb
    * @param userId userId
    */

  requestPermission(userId) {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        this.token = token;
        console.log(token);
        this.updateToken(userId, token);
      },
      (err) => {
        console.error('Insufficient Permission to notify.', err);
      }
    );
  }

  recieveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        console.log('new message recieved. ', payload);
        this.currentMessage.next(payload);
      }
    );
  }

  getToken() {
    return this.token;
  }
}
