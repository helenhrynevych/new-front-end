import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { User } from '../_models/user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Car } from '../_models/car';
import { CarService } from '../_services/car.service';


@Component({
  selector: 'app-add-new-car',
  templateUrl: './add-new-car.component.html',
  styleUrls: ['./add-new-car.component.css']
})
export class AddNewCarComponent implements OnInit {

  loading = false;
  submitted = false;
  currentUser: User;

  name: string;
  model: string;
  serialNumber: number;
  volume: number;
  color: string;
  year: number;
  city: string;
  country: string;
  about: string;


  constructor(
    private router: Router,
    private carService: CarService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.userService.getByUsername(localStorage.getItem('currentUsername')).pipe().subscribe(user => this.currentUser = user);
  }

  addNewCar() {
    this.submitted = true;

    const carToAdd = new Car(
      0,
      this.currentUser.id,
      this.name,
      this.model,
      this.serialNumber,
      this.volume,
      this.color,
      this.year,
      this.city,
      this.country,
      this.about

    );

    this.carService.addCar(carToAdd).subscribe(
      data => {
        this.router.navigate(['/']);
      },
      error => {
          console.log(error);
          this.loading = false;
      });

    this.loading = true;


  }



}
