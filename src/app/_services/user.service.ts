﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { User } from '../_models/user';
import { UserRegistration } from '../_models/userModelForRegister';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class UserService {

    headers = new Headers ({'Authorization': 'Client-ID a91d10bd0d1b991'});

    constructor(private http: HttpClient) { }


    getByUsername(username: string): Observable<User> {
        let param = new HttpParams().set('username', username);
        return this.http.get<User>(environment.url + '/api/user/get', {params: param});
    }

    register(user: UserRegistration) {
        return this.http.post(environment.url + '/api/auth/signup', user);
    }

    update(user: User): Observable<any>{
        return this.http.put(environment.url + '/api/user/edit', user);
    }
    // storeImage(image) {
    //     this.http.post(environment.url, image, {headers: this.headers})
    //                 .toPromise()
    //                 .then(response => {
    //                     console.log(response);
    //                 });
    // }

    delete(id: number) {
    }
}