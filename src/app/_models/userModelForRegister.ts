﻿export class UserRegistration {
    firstName: string;
    lastName: string;
    username: string;
    email: string;
    role:string [];
    password: string;    
    constructor(
        firstName: string,
        lastName: string,
        username: string,
        email: string,
        password: string, 
        role:string []
    ){
        this.firstName=firstName;
        this.lastName=lastName;
        this.username=username;
        this.email=email;
        this.role=role;
        this.password=password;


    }
}