export class Car{
    id:number;
    userId:number;
    name:string;
    photo:string;
    model:string;
    volume:number;
    color:string;
    serialNumber:number;
    year:number;
    characteristics:string;
    city:string;
    country:string;
    combustion:number;
    time: number;
    horsepower:number;
    mileage:number;
    constructor(
        id:number,
        userId:number,
        name:string,
        model:string,
        serialNumber:number,
        volume:number,
        color:string,
        year:number,
        city:string,
        country:string,
        characteristics:string
    )
    {
        this.id = id;
        this.userId = userId;
        this.name  = name;
        this.model = model;
        this.serialNumber = serialNumber;
        this.volume = volume;
        this.color = color;
        this.year = year;
        this.city = city;
        this.country = country;
        this.characteristics = characteristics;
    }
}
