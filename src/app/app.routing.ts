﻿import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_guards';
import { CarDetailsComponent } from './car-details/car-details.component';
import { MapComponent } from './map';
import { NotifactionFormComponent } from './notifaction-form/notifaction-form.component';
import { AddCarHistoryComponent } from './add-car-history/add-car-history.component';
import { AddNewCarComponent } from './add-new-car/add-new-car.component';
import { HistoryTableComponent } from './history-table/history-table.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'user', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'details/:id', component: CarDetailsComponent, canActivate: [AuthGuard] },
    { path: 'map', component: MapComponent, canActivate: [AuthGuard] },
    { path: 'addhistory', component: AddCarHistoryComponent },
    { path: 'addcar', component: AddNewCarComponent },
//    { path: '**', redirectTo: '' },
    { path: 'addnotification', component: NotifactionFormComponent, canActivate: [AuthGuard] },
    { path: 'history', component: HistoryTableComponent,  canActivate: [AuthGuard] },

];

export const routing = RouterModule.forRoot(appRoutes);
