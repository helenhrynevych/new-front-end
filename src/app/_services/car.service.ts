import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Car } from '../_models/car';

import { environment } from '@environments/environment';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CarService {

 // private url = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  addCar(car: Car): Observable<any> {
    return this.http.post(environment.url + '/car/add', car);
  }

  getCarListByUserId(id: number): Observable<any> {
    return this.http.get<Car[]>(environment.url + '/car/getList/' + id);
  }

  getCarById(carId: string): Observable<any> {
    let p = new HttpParams().set('id', carId);
    return this.http.get(environment.url + '/car/get', {params: p});
  }

  editCar(car: Car): Observable<any> {
    return this.http.put(environment.url + '/car/edit', car);
  }

}
