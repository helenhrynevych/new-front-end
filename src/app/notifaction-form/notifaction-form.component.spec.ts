import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifactionFormComponent } from './notifaction-form.component';

describe('NotifactionFormComponent', () => {
  let component: NotifactionFormComponent;
  let fixture: ComponentFixture<NotifactionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NotifactionFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifactionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
