export class User {
    id: any;
    firstname: string;
    username: string;
    email: string;
    password: string;
    roles:string [];
    lastname: string;
    buildingNumber: string;
    apartamentNumber: string;
    city: string;
    country: string;
    about: string;
    photo: string;
    background: string;
    address: string;
    postalCode: string;
    quotation: string;    
    constructor(){}
}