﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService, AuthenticationService, AlertService } from '@app/_services';
import { UserRegistration } from '../_models/userModelForRegister';

@Component({templateUrl: 'register.component.html'})
export class RegisterComponent implements OnInit {

    loading = false;
    submitted = false;

    firstName: string;
    lastName: string;
    username: string;
    email: string;
    password: string;
    confirmPassword: string;
    role=['user','pm'];


    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private alertService: AlertService
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.isAuthenticated()) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
    }


    onSubmit() {
        this.submitted = true;

        if(this.password !== this.confirmPassword ){
            console.log('Passwords are different');
            this.loading = false;
            return;
        }

        const userToRegister: UserRegistration = new UserRegistration(
            this.firstName,
            this.lastName,
            this.username,
            this.email,
            this.password,
            this.role
        );
        console.log('heere :');
        console.log(userToRegister);

        this.loading = true;
        this.userService.register(userToRegister)
            .subscribe(
                data => {
                    //powinno tu działać ;o
                     this.alertService.success('Registration successful', false);
                    // this.router.navigate(['/login']);
                    console.log('Success');
                },
                error => {
                    this.router.navigate(['/login']);
                    this.alertService.success('Registration successful', false);
                    this.loading = false;
                });
    }
}
