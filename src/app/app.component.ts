﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorage } from '../app/_helpers/token.storage';
import { AuthenticationService } from './_services';
import { MessagingService } from "./_services/messaging.service";


@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent {
    title = 'products';
    message;

    
    constructor(
        private msgService: MessagingService,
        private router: Router,
        private authenticationService: AuthenticationService,
        private token: TokenStorage
    ) {};

    ngOnInit() {
        const userId = 'user001'; 
        this.msgService.requestPermission(userId);
        this.msgService.recieveMessage();
        this.message = this.msgService.currentMessage;
      }

    logout() {
       
        this.token.signOut();
        location.reload();    
    }
}
