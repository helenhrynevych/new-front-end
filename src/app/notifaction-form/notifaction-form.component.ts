import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NotificationService } from '../_services/notification.service';
import { Notification } from '../_models/notification';
import { MessagingService } from '../_services/messaging.service';
import mockNotification from '../_helpers/mock-notifaction';

@Component({
  selector: 'app-notifaction-form',
  templateUrl: './notifaction-form.component.html',
  styleUrls: ['./notifaction-form.component.sass']
})
export class NotifactionFormComponent implements OnInit {
  notificationContent: string;
  constructor(private toastr: ToastrService, private notificationService: NotificationService, private messageService: MessagingService) {}

  ngOnInit() {}

  submitNotifiaction() {
    if (this.notificationContent) {
      const notifaction = new Notification(this.notificationContent, this.messageService.getToken());
      this.notificationService.sendNotification(notifaction);
    } else {
      this.notificationError();
    }
  }
  mockNotificationLocal() {
    if (this.notificationContent) {
      this.toastr.success('Wysłano notyfikacje!');
      mockNotification(`hej!`);
    } else {
      this.notificationError();
    }
  }
  notificationError() {
    this.toastr.error('Wiadomość nie może być pusta!');
  }
}
