import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import {History} from '../_models/history';
import { Observable } from 'rxjs';

import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})

export class CarHistoryService {
  //private url = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  addHistory(history: History): Observable<any>  {
    return this.http.post(environment.url + '/history/add', history);

  }

  getHistoryListByUserId(id: number): Observable<any> {
    return this.http.get<History[]>(environment.url + '/history/getList/' + id);
  }

  //todo
  getHistoryById( historyId: string): Observable<any> {
    let p = new HttpParams().set('HistoryId', historyId);
    return this.http.get<History>(environment.url + '/history/get', {params: p});

  }
}
