import { ViewChild, NgZone } from '@angular/core';
/// <reference types="@types/googlemaps" />
import { Component } from '@angular/core';
import { async } from 'q';


@Component({ templateUrl: 'map.component.html' })
export class MapComponent {  
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  isTracking = false;
  marker: google.maps.Marker;

  currentLat: any;
  currentLong: any;
  latitude: any;
  longitude: any;
  markers:Array<any>=[];
  isKM:any=5000;
  isType:any="";

  constructor(private ngZone: NgZone){}
  ngOnInit() {
    this.findMe();
    this.initMap()
  }

  findMe() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
        this.showPosition(position);
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  initMap() {
    let mapProp = {
      center: new google.maps.LatLng(50, 19),
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    this.findMe(); 
  }


  showPosition(position) {
    this.currentLat = position.coords.latitude;
    this.currentLong = position.coords.longitude;

    let location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    this.map.panTo(location);

    if (!this.marker) {
      this.marker = new google.maps.Marker({
        position: location,
        map: this.map,
        title: 'YOU'
      });
    }
    else {
      this.marker.setPosition(location);
    }
  }

   getCarWash(){
    let service = new google.maps.places.PlacesService(this.map);
    service.nearbySearch({
              location: new google.maps.LatLng(this.currentLat,this.currentLong),
              radius: this.isKM,
              types: ['car_wash']
            }, (results, status) => {
                this.callback(results, status);
            });
  }

  getParking(){
    let service = new google.maps.places.PlacesService(this.map);
    service.nearbySearch({
              location: new google.maps.LatLng(this.currentLat,this.currentLong),
              radius: this.isKM,
              types: ['parking']
            }, (results, status) => {
                this.callback(results, status);
            });
  }


  getMechanic(){
    let service = new google.maps.places.PlacesService(this.map);
    service.nearbySearch({
              location: new google.maps.LatLng(this.currentLat,this.currentLong),
              radius: this.isKM,
              types: ['car_repair']
            }, (results, status) => {
                this.callback(results, status);
            });
  }

  getCarDealer(){
    let service = new google.maps.places.PlacesService(this.map);
    service.nearbySearch({
              location: new google.maps.LatLng(this.currentLat,this.currentLong),
              radius: this.isKM,
              types: ['car_dealer']
            }, (results, status) => {
                this.callback(results, status);
            });
  }

  getGasStation(){
    let service = new google.maps.places.PlacesService(this.map);
    service.nearbySearch({
              location: new google.maps.LatLng(this.currentLat,this.currentLong),
              radius: this.isKM,
              types: ['gas_station']
            }, (results, status) => {
                this.callback(results, status);
            });
  }

  callback(results, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
      if(this.markers != null){
      for (var i = 0; i < this.markers.length; i++) {
        this.markers[i].setMap(null);       }
       this.markers = [];
      }
      
      for (var i = 0; i < results.length; i++) {
        this.createMarker(results[i]);
      }
    }
  }

  createMarker(place){
    this.markers.push( new google.maps.Marker({
        map: this.map,
        position: place.geometry.location,
        title: place.name
    }));

    let infowindow = new google.maps.InfoWindow();

    google.maps.event.addListener(this.markers, 'click', () => {
      this.ngZone.run(() => {
        infowindow.setContent(place.name);
        infowindow.open(this.map, this.marker);
      });
    });
  }
}
