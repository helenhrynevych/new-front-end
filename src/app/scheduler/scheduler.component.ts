import {Component, ViewChild, AfterViewInit} from '@angular/core';
import {DayPilotSchedulerComponent} from "daypilot-pro-angular";
import { DataService, CreateEventParams } from '../_services/data.service'
import { User } from '@app/_models/user';
import { UserService } from '../_services/user.service'
import { NotificationService } from '@app/_services/notification.service';
import { Notification } from '@app/_models/notification';
import { MessagingService } from '@app/_services/messaging.service';
import mockNotification from '../_helpers/mock-notifaction';

@Component({
    selector: 'scheduler-component',
    template: `
    <div class="row">
      <div class="col-md-12">
          <div class="card">
              <div class="header">
              <h4 class="title">Scheduler<h4>
              <p class="category">Here you can choose day for the next vehicle check</p>

              <div class="content table-responsive table-full-width">
              <daypilot-scheduler [config]="config" [events]="events" #scheduler1></daypilot-scheduler>
    `,
    styles: [`.body { padding: 10px; }`]
})
export class SchedulerComponent implements AfterViewInit {
  @ViewChild("scheduler1")
  scheduler: DayPilotSchedulerComponent;
  currentUser: User;
  events: any;

  constructor(private ds: DataService,
              private userService: UserService,
              private notyfikacje: NotificationService,
              private messageservice: MessagingService) {}

  ngAfterViewInit(): void {
    this.ds.getResources().subscribe(result => this.config.resources = result);
    this.userService.getByUsername(localStorage.getItem('currentUsername')).subscribe(user => {
      this.currentUser = user; 

      this.ds.getEvents(user.id).subscribe(result => {
        this.events = result;
        var oneDay = 24*60*60*1000;

        result.forEach(entry => {
        
        var firstDate = new Date(entry.start);
        var secondDate = new Date();
        var diffDays = Math.round((firstDate.getTime() - secondDate.getTime()/(86400000)));
        var diff = (firstDate.getTime() - secondDate.getTime())/86400000; 
        var round = Math.round(diff);

        console.log(entry.resource.name);
        if( round < 11 ){
          if (entry.resource === 2){
             //this.notyfikacje.sendNotification(new Notification('ubezpieczenie skonczy sie za '+round+' dni,w '+result.text,this.messageservice.getToken()));
            mockNotification('ubezpieczenie skonczy sie za '+round+' dni,w '+entry.text);
            }else {
            //this.notyfikacje.sendNotification(new Notification('przeglad skonczy sie za '+round+'dni,w '+result.text,this.messageservice.getToken()));
            mockNotification('przeglad skonczy sie za '+round+'dni,w '+entry.text);
          } 
         }
        })  
       })
      });
    }

  config: any = {
    timeHeaders : [
      {groupBy: "Month", format: "MMMM yyyy"},
      {groupBy: "Day", format: "d"}
    ],
    days: 365,
    startDate: "2019-02-01",
    scale: "Day",
    onTimeRangeSelected: args => {
      let name = prompt("New event name:", "Event");
      this.scheduler.control.clearSelection();
      if (!name) {
        return;
      }
      let params: CreateEventParams = {
        start: args.start.toString(),
        end: args.end.toString(),
        text: name,
        resource: args.resource,
        user: this.currentUser.id
      };
      this.ds.createEvent(params).subscribe(result => {
        this.events.push(result);
        this.scheduler.control.message("Event created");
      } );
    },
    onEventMove: args => {
        let params: MoveEventParams = {
          id: args.e.id(),
          start: args.newStart.toString(),
          end: args.newEnd.toString(),
          resource: args.newResource
        };
        this.ds.moveEvent(params).subscribe(result => {
          this.scheduler.control.message("Event moved");
        });
      }
  };

}

export interface MoveEventParams {
    id: string | number;
    start: string;
    end: string;
    resource: string | number;
  }