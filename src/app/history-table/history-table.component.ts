import { Component, OnInit } from '@angular/core';

import {CarHistoryService} from '../_services/car-history.service';
import {UserService} from '../_services/user.service';
import { User } from '@app/_models/user';
import { FormBuilder } from '@angular/forms';
declare interface TableData {
  headerRow: string[];
}


@Component({
  selector: 'app-history-table',
  templateUrl: './history-table.component.html',
  styleUrls: ['./history-table.component.css']
})
export class HistoryTableComponent implements OnInit {

  historyList: History[];
  currentUser: User;

  public tableHistory: TableData;

  constructor(
    private carHistoryService: CarHistoryService,
    private userService: UserService
  ) { }

  ngOnInit() {

    this.tableHistory = {
      headerRow: [  'Company', 'Title',  'Date', 'Price']
  };
    this.getUser();
  }

  getUser() {
    this.userService.getByUsername(localStorage.getItem('currentUsername')).pipe().subscribe(user => {
      this.currentUser = user;
      this.getHistoryList(user.id);

    });
  }

  getHistoryList(id: number) {
    this.carHistoryService.getHistoryListByUserId(id)
    .subscribe(data => {
      this.historyList = data;
    });
  }

}
