// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCvlrXAgi-D0uO-ExZbjSaUlfl8bbRnYqw",
    authDomain: "fit-entity-229020.firebaseapp.com",
    databaseURL: "https://fit-entity-229020.firebaseio.com",
    projectId: "fit-entity-229020",
    storageBucket: "fit-entity-229020.appspot.com",
    messagingSenderId: "66295675022"
  },
  backendUrl: 'http://localhost:8080',
  apiUrl: 'http://localhost:4000',
 // url : 'http://40.127.168.54:8080'
  url : 'http://localhost:8080',
  imgURL: null
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
