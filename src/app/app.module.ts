﻿import { CUSTOM_ELEMENTS_SCHEMA,NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
// used to create fake backend
import { NguiMapModule} from '@ngui/map';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { AlertComponent } from './_components';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { TokenStorage } from './_helpers/token.storage';
import { CarsComponent } from './cars/cars.component';
import { CarDetailsComponent } from './car-details/car-details.component';
import { MapComponent } from './map';
import { AddCarHistoryComponent } from './add-car-history/add-car-history.component';
import { AddNewCarComponent } from './add-new-car/add-new-car.component';
import { MessagingService} from './_services/messaging.service';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { NotifactionFormComponent } from './notifaction-form/notifaction-form.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { ToastrModule } from 'ngx-toastr';
import { HistoryTableComponent } from './history-table/history-table.component'
import {DayPilotModule} from "daypilot-pro-angular";
import {SchedulerModule} from "./scheduler/scheduler.module";




@NgModule({
    imports: [
        SchedulerModule,
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        DayPilotModule,
        routing,
        FormsModule,
        AngularFireModule.initializeApp(environment.firebase, 'MyCar'),
        AngularFireDatabaseModule,
        AngularFireMessagingModule,
        AngularFireAuthModule,
        ToastrModule.forRoot({
           timeOut: 3000,
           progressBar: true,
           extendedTimeOut: 2000,
           easeTime: 500,
           resetTimeoutOnDuplicate: true
        }),
        NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyCHCzyd2EPTUF75FGr4r8fnQ1THNdQ7B2A'})

    ],
    declarations: [
        AppComponent,
        AlertComponent,
        NotifactionFormComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        CarDetailsComponent,
        CarsComponent,
        MapComponent ,
        AddNewCarComponent,
        HistoryTableComponent,
        AddCarHistoryComponent
    ],
        
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        MessagingService,
        TokenStorage
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [AppComponent]
})

export class AppModule { }
export class MonthlyCalendarModule { }